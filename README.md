# NextJS Dockerised

Services:

- Port 8000 - NextJS application with Prisma preinstalled.
- Port 8001 - phpMyAdmin
- Port 8002 - Swagger - Edit data/swagger/dist/swagger.yml
- Port 6033 - MySQL Database
